mapa-cultura
============

Muestra los puntos de interés cultural de la república argentina. El proyecto utiliza la siguiente tecnología:

- JavaScript

- Leaflet.js

- Carto

- JavaScript


Podés ver la aplicación corriendo en http://opensas.github.io/mapa-cultura/
